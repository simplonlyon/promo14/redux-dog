import React from 'react';
import 'antd/dist/antd.css';
import { DogList } from './components/DogList';


function App() {
  return (
    <div>
      <DogList />
    </div>
  );
}

export default App;
