import { Col, Row } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchDogs } from "../app/dogSlice";
import { Dog } from "./Dog";



export function DogList() {
    const dispatch = useDispatch();
    const dogs = useSelector(state => state.dogs.list);

    useEffect(() => {

        dispatch(fetchDogs());

    }, [dispatch]);

    return (
        <Row gutter={10}>
            {dogs.map(item =>
                <Col key={item.id} span={6}>
                    <Dog dog={item} />
                </Col>
            )}

        </Row>
    );
}