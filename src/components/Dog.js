import { Card } from "antd";



export function Dog({ dog }) {

    return (
        <Card
            hoverable
            cover={<img alt={dog.name} src={dog.picture} />}
        >
            <Card.Meta title={dog.name} description={dog.breed} />
        </Card>
    );
}