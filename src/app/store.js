import { configureStore } from '@reduxjs/toolkit';
import dogSlice from './dogSlice';

export const store = configureStore({
  reducer: {
    dogs: dogSlice
  },
});
