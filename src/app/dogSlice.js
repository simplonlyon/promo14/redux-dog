import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";


const dogSlice = createSlice({
    name: 'dogs',
    initialState: {
        list: []
    },
    reducers: {
        setDogs(state, {payload}) {
            state.list = payload;
        },
        addDog(state, {payload}) {
            state.list.push(payload);
        }
    }
});

export const {addDog,setDogs} = dogSlice.actions;

export default dogSlice.reducer;

export const fetchDogs = () => async (dispatch) => {

    try {
        const response = await axios.get('http://localhost:8000/api/dog');

        dispatch(setDogs(response.data));
    } catch (error) {
        console.log(error);
    }
}